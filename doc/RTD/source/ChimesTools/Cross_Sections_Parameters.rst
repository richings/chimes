.. Cross Sections Parameters
   Alexander Richings, 3rd June 2020

.. _CrossSectionsParameters_label: 

Parameters
----------

The following parameters can be specified in the parameter file passed to ``generate_cross_sections.py``. If a parameter is not given in the parameter file, it uses a default value as defined in the ``read_parameter_file()`` function within ``generate_cross_sections.py``. 

+-------------------------------------+------------------------------------------------------------------------------+
| Parameter                           | Description                                                                  |
+=====================================+==============================================================================+
| ``chimes_main_data_path``           | | Path to the ``chimes_main_data.hdf5`` data file from the ``chimes-data``   |
|                                     | | repository.                                                                |
|                                     |                                                                              |
+-------------------------------------+------------------------------------------------------------------------------+
| ``cross_sections_data_path``        | | Path to the directory containing additional data files needed by           |
|                                     | | ``generate_cross_sections.py`` (for example, the frequency-dependent       |
|                                     | | cross sections for each species). These can be found in                    |
|                                     | | ``chimes-tools/generate_cross_sections/data/``.                            |
|                                     |                                                                              |
+-------------------------------------+------------------------------------------------------------------------------+
| ``spectrum_file``                   | | Name of the text file that defines the shape of the UV spectrum. This      |
|                                     | | needs to contain a list of data points specifying log10(E [Ryd]) in the    |
|                                     | | first column and log10(J_nu [erg/s/sr/cm^2/Hz]) in the second column,      |
|                                     | | where E and J_nu are the photon energy and specific intensity              |
|                                     | | respectively. Lines that start with a ``#`` character are treated as       |
|                                     | | comments and are ignored. We include an example that can be used as a      |
|                                     | | template (see the :ref:`CrossSectionsExample_label` section).              |
|                                     |                                                                              |
+-------------------------------------+------------------------------------------------------------------------------+
| ``output_file``                     | | Name of the output cross sections HDF5 file.                               |
|                                     |                                                                              |
+-------------------------------------+------------------------------------------------------------------------------+
| ``spectrum_shape``                  | | A string describing the shape of the spectrum that was used. This is       |
|                                     | | added to the header information in the output cross sections file.         |
|                                     |                                                                              |
+-------------------------------------+------------------------------------------------------------------------------+
| ``version_date``                    | | A string giving the date when the cross sections file was created. This    |
|                                     | | is added to the header information in the output file.                     |
|                                     |                                                                              |
+-------------------------------------+------------------------------------------------------------------------------+
| ``rt_coupling_flag``                | | Integer flag (``0`` or ``1``) that determines whether RT coupling is       |
|                                     | | enabled. This affects the photon energy range over which the cross         |
|                                     | | sections and photon energies are defined (see the                          |
|                                     | | :ref:`CoupleRT_label` section for details). This flag is                   |
|                                     | | also added to the header information in the output file.                   |
|                                     |                                                                              |
+-------------------------------------+------------------------------------------------------------------------------+
| | ``rt_HII_``                       | | Integer flag (``0`` or ``1``) that specifies whether photons from the      |
| | ``recombination_flag``            | | recombination of HII are included in this energy bin. Only used if         |
|                                     | | if RT coupling is switched on. This flag is also added to the header       |
|                                     | | information in the output file.                                            |
|                                     |                                                                              |
+-------------------------------------+------------------------------------------------------------------------------+
| | ``rt_HeII_``                      | | Integer flag (``0`` or ``1``) that specifies whether photons from the      |
| | ``recombination_flag``            | | recombination of HeII are included in this energy bin. Only used if        |
|                                     | | if RT coupling is switched on. This flag is also added to the header       |
|                                     | | information in the output file.                                            |
|                                     |                                                                              |
+-------------------------------------+------------------------------------------------------------------------------+
| ``rt_E_min_eV``                     | | Minimum photon energy in this energy bin, in eV. Only used if RT           |
|                                     | | coupling is switched on. This parameter is also added to the header        |
|                                     | | information in the output file.                                            |
|                                     |                                                                              |
+-------------------------------------+------------------------------------------------------------------------------+
| ``rt_E_max_eV``                     | | Maximum photon energy in this energy bin, in eV. Only used if RT           |
|                                     | | coupling is switched on. This parameter is also added to the header        |
|                                     | | information in the output file.                                            |
|                                     |                                                                              |
+-------------------------------------+------------------------------------------------------------------------------+
