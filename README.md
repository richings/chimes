# CHIMES

Welcome to the main CHIMES repository. This contains the source code for the CHIMES module. 

For more information on CHIMES, please see the [main CHIMES home page](https://richings.bitbucket.io/chimes/home.html).

For more information on how to use CHIMES, please see the [CHIMES User Guide](https://richings.bitbucket.io/chimes/user_guide/index.html).
